export default {
    template: `
    <korisnik-forma v-on:sacuvaj="create"  v-bind:tekst="'Registruj se'"></korisnik-forma>
    `,
    data() {
        return {
            korisnici: [],
        }
    },
    methods: {
        refreshKorisnici() {
            axios.get("api/korisnici").then((response) => {
                this.korisnici = response.data;
            });
        },
        create(korisnik) {
            axios.post("api/korisnici", korisnik).then((response) => {
                this.$router.push("/");
                this.refreshKorisnici();
                
            });
        }
    },
    created() {
        this.refreshKorisnici();
        
    }
}

