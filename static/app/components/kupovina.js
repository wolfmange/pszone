export default {
    template: `
<form v-on:submit.prevent="update" class="form-controll">
    <div class="mb-3">
    <label class="form-label">ID Konzole:</label>
    <input type="int" class="form-control" v-model="kupovina.konzola_id" required>
    </div>
    <div class="mb-3">
    <label class="form-label">Kolicina:</label>
    <input type="int" class="form-control" v-model="kupovina.kolicina" required>
    </div>
    <div class="mb-3">
    <label class="form-label">Datum(YYYY-MM-DD):</label>
    <input type="datetime" class="form-control" v-model="kupovina.datumKupovine" :placeholder="placeholder" value="" required>
    </div>
    <div class="mb-3">
    <input class="btn btn-primary" type="submit" value="Izmeni"> 
    </div>
</form>
    `,
    data() {
        return {
            kupovina: {},
            placeholder: 'U formatu YYYY-MM-DD'
            
        }
    },
    methods: {
        refresh() {
            axios.get(`api/kupovine/${this.$route.params['id']}`).then((response) => {
                this.kupovina = response.data;
            });
        },
        update() {
            axios.put(`api/kupovine/${this.$route.params['id']}`, this.kupovina).then((response) => {
                this.$router.push("/kupovine");
            });
        }
    },
    created() {
        this.refresh();
    }
}