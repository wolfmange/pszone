export default {
    template: `
    <h3>Welcome</h3>
    <h3>to</h3>
    <h3 style="font-size: 100px;">PsZone</h3>
    <div style="background-color: white">
    </div>

    <div class="alert alert-danger" role="alert" v-if="neuspesanLogin">
  Neuspesna prijava na sistem!
</div>
    <form v-on:submit.prevent="login()" class="form-controll">
  <div class="mb-3">
    <label class="form-label">Korisničko ime</label>
    <input type="text" v-model="korisnik.korisnicko_ime" class="form-control" required>
  </div>
  <div class="mb-3">
    <label class="form-label">Lozinka</label>
    <input type="password" v-model="korisnik.lozinka" class="form-control" required>
  </div>
  <button type="submit" class="btn btn-primary">Login</button>
  <div class="mb-3">
  <p>Nemate nalog? Registrujte se <router-link style="color: Blue;" to="/registracija">ovde</router-link></p>
  </div>
</form>
    `,
    data: function() {
        return {
            korisnik: {
                "korisnicko_ime": "",
                "lozinka": ""
            },
            neuspesanLogin: false
        };
    },
    methods: {
        login: function() {
            axios.post(`api/login`, this.korisnik).then((response) => {
                localStorage.setItem("token", response.data);
                this.$router.push("/korisnici");
            }, _ => {
                this.neuspesanLogin = true;
            });
        }
    }
}