export default {
    template: `
<form v-on:submit.prevent="update" class="form-controll">
    <div class="mb-3">
    <label class="form-label">Naziv:</label>
    <input type="text" class="form-control" placeholder="Naziv konzole" v-model="konzola.naziv" required>
    </div>
    <div class="mb-3">
    <label class="form-label">Opis:</label>
    <textarea class="form-control" v-model="konzola.opis"></textarea>
    </div>
    <div class="mb-3">
    <label class="form-label">Cena:</label>
    <input type="number" class="form-control" v-model="konzola.cena" required>
    </div>
    <div class="mb-3">
    <input class="btn btn-primary" type="submit" value="Izmeni"> 
    </div>
</form>
    `,
    data() {
        return {
            konzola: {}
        }
    },
    methods: {
        refresh() {
            axios.get(`api/konzole/${this.$route.params['id']}`).then((response) => {
                this.konzola = response.data;
            });
        },
        update() {
            axios.put(`api/konzole/${this.$route.params['id']}`, this.konzola).then((response) => {
                this.$router.push("/konzole");
            });
        }
    },
    created() {
        this.refresh();
    }
}