export default {
    props: ["kupovine"],
    emits: ["izmena", "uklanjanje"],
    data() {
        return {}
    },
    template: `
<table class="table table-hover table-light">
<thead class="thead-dark">
    <tr>
        <th>ID</th>
        <th>ID Kupca</th>
        <th>ID Konzole</th>
        <th>Kolicina</th>
        <th>Datum Kupovine</th>
        <th></th>
    </tr>
    
</thead>
<tbody>
    <tr v-for="kupovina in kupovine">
            <td>{{kupovina.id}}</td>
            <td>{{kupovina.korisnik_id}}</td>
            <td>{{kupovina.konzola_id}}</td>
            <td>{{kupovina.kolicina}}</td>
            <td>{{kupovina.datumKupovine}}</td>
            <td><button class="btn btn-primary me-3" v-on:click="$emit('izmena', {...kupovina})">Izmeni</button><button
                    v-on:click="$emit('uklanjanje', kupovina.id)" class="btn btn-danger">Ukloni</button></td>

    </tr>

</tbody>
</table>
    `
}