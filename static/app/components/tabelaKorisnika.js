export default {
    props: ["korisnici"],
    emits: ["izmena", "uklanjanje"],
    data() {
        return {}
    },
    template: `
<table class="table table-hover table-dark">
<thead>
    <tr>
        <th>ID</th>
        <th>Korisnicko ime</th>
        <th>Ime</th>
        <th>Prezime</th>
        <th></th>
    </tr>
</thead>
<tbody>
    <tr v-for="korisnik in korisnici">
        <td>{{korisnik.id}}</td>
        <td>{{korisnik.korisnicko_ime}}</td>
        <td>{{korisnik.ime}}</td>
        <td>{{korisnik.prezime}}</td>
        <td><button class="btn btn-primary me-3" v-on:click="$emit('izmena', {...korisnik})">Izmeni</button><button
                v-on:click="$emit('uklanjanje', korisnik.id)" class="btn btn-danger">Ukloni</button></td>
    </tr>
</tbody>
</table>
    `
}