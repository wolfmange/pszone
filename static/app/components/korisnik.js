export default {
    template: `
<form v-on:submit.prevent="update" class="form-controll">
    <div class="mb-3">
    <label class="form-label">Korisnicko ime</label>
    <input type="text" class="form-control" placeholder="Naziv korisnici" v-model="korisnik.korisnicko_ime" required>
    </div>
    <div class="mb-3">
    <label class="form-label">Lozinka:</label>
    <textarea class="form-control" v-model="korisnik.lozinka" required></textarea>
    </div>
    <div class="mb-3">
    <label class="form-label">Ime:</label>
    <input type="text" class="form-control" v-model="korisnik.ime" required>
    </div>
    <label class="form-label">Prezime</label>
    <input type="text" class="form-control" v-model="korisnik.prezime" required>
    </div>
    <div class="mb-3">
    <input class="btn btn-primary" type="submit" value="Izmeni"> 
    </div>
</form>
    `,
    data() {
        return {
            korisnik: {},
        }
    },
    methods: {
        refresh() {
            axios.get(`api/korisnici/${this.$route.params['id']}`).then((response) => {
                this.korisnik = response.data;
            });
        },
        update() {
            axios.put(`api/korisnici/${this.$route.params['id']}`, this.korisnik).then((response) => {
                this.$router.push("/korisnici");
            });
        }
    },
    created() {
        this.refresh();
    }
}