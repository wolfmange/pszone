export default {
    template: `
<div>
    <h1 style="color: white; margin-top: 20px;">Korisnici</h1>
    <h2>Stranica za prikaz svih prijavljenih korisnika</h2>
    <h2>Moguća izmena i uklanjanje</h2>
    
    <tabela-korisnika v-bind:korisnici="korisnici" v-on:uklanjanje="remove" v-on:izmena="setZaIzmenu"></tabela-korisnika>
</div>
    `,
    data() {
        return {
            korisnici: [],
            
        }
    },
    methods: {
        setZaIzmenu(korisnik) {
            this.$router.push(`/korisnici/${korisnik.id}`);
        },
        refreshKorisnici() {
            axios.get("api/korisnici").then((response) => {
                this.korisnici = response.data;
            });
        },
        update(korisnik) {

            axios.put(`api/korisnici/${korisnik.id}`, korisnik).then((response) => {
                this.refreshKorisnici();
            });
        },
        remove(id) {
            axios.delete(`api/korisnici/${id}`).then((response) => {
                this.refreshKorisnici();
            });
        }
    },
    created() {
        this.refreshKorisnici();
    }
}