export default {
    template: `
    <h2 style="margin-left: 100px;">Unesite sve potrebne informacije da biste kupili proizvod</h2>
    <kupovina-forma v-on:sacuvaj="create"  v-bind:tekst="'Kupi'"></kupovina-forma>
    `,
    data() {
        return {
            kupovine: [],
        }
    },
    methods: {
        refreshKupovine() {
            axios.get("api/kupovine").then((response) => {
                this.kupovine = response.data;
            });
        },
        create(kupovina) {
            axios.post("api/kupovine", kupovina).then((response) => {
                this.$router.push("/kupovine");
                this.refreshKorisnici();
                
            });
        }
    },
    created() {
        this.refreshKupovine();
        
    }
}