export default {
    props: ["kupovina", "tekst", "korisnik"],
    emits: ["sacuvaj"],
    data() {
        return {
            novaKupovina: this.kupovina ? {...this.kupovina} : {}
        }
    },
    watch: {
         kupovina: function(newValue, oldValue) {
             this.novaKupovina = {...this.kupovina};
         }
    },
    template: `
    <form class="form-controll" v-on:submit.prevent="$emit('sacuvaj', {...novaKupovina})">
        <div class="mb-3">
            <label class="form-label">Vas ID</label> 
            <input type="int" v-model="novaKupovina.korisnik_id" class="form-control" required>
        </div>
        <div class="mb-3">
        <label class="form-label">ID Konzole</label> 
            <input type="int" v-model="novaKupovina.konzola_id" class="form-control" required>
        </div>
        <div class="mb-3">
        <label class="form-label">Kolicina</label> 
            <input type="int" v-model="novaKupovina.kolicina" class="form-control" required>
        </div>
        <div class="mb-3">
            <label class="form-label">Datum kupovine(YYYY-MM-DD)</label> 
            <input type="datetime" v-model="novaKupovina.datumKupovine" class="form-control" required>
        </div>

        <div class="mb-3" style="margin-left: 250px;">
            <input type="submit" v-bind:value="tekst">
        </div>
    </form>
    `
}