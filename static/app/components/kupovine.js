export default {
    template: `
<div>
    <h1 style="color: white; margin-top: 20px;">Kupovine</h1>
    <h2>Pregled prethodnih kupovina</h2>
    <h2>Moguća izmena i uklanjanje</h2>
    
    <tabela-kupovina v-bind:kupovine="kupovine" v-on:uklanjanje="remove" v-on:izmena="setZaIzmenu"></tabela-kupovina>
    <div>
        <img src="ps.png" class="img-fluid" >
    </div>
</div>
    `,
    data() {
        return {
            kupovine: [],
        }
    },
    methods: {
        refreshKupovine() {
            axios.get("api/kupovine").then((response) => {
                this.kupovine = response.data;
            });
        },
        
        create(kupovina) {
            
            axios.post("api/kupovine", kupovina).then((response) => {
                this.refreshKupovine();
            });
        },
        update(kupovina) {
            axios.put(`api/kupovine/${kupovina.id}`, kupovina).then((response) => {
                this.refreshKupovine();
            });
        },
        remove(id) {
            axios.delete(`api/kupovine/${id}`).then((response) => {
                this.refreshKupovine();
            });
        },
        setZaIzmenu(kupovina) {
            this.$router.push(`/kupovine/${kupovina.id}`);
        }
    },
    created() {
        this.refreshKupovine();
    }
}