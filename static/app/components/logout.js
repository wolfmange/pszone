export default {
    template: ``,
    methods: {
        logOut() {
            localStorage.removeItem("token");
            this.$router.push("/");
        }
    },
    created() {
        this.logOut();
    }
}