export default {
    props: ["konzole", "generacije"],
    emits: ["izmena", "uklanjanje"],
    data() {
        return {}
    },
    template: `
<table class="table table-hover table-light">
<thead class="thead-dark">
    <tr>
        <th>ID</th>
        <th>Generacija</th>
        <th>Naziv</th>
        <th>Opis</th>
        <th>Cena</th>
        <th></th>
    </tr>
    </tr>
</thead>
<tbody>
    <tr v-for="konzola in konzole">
            <td>{{konzola.id}}</td>
            <td>{{konzola.generacija_id}}</td>
            <td>{{konzola.naziv}}</td>
            <td>{{konzola.opis}}</td>
            <td>{{konzola.cena}}</td>
            <td><button class="btn btn-primary me-3" v-on:click="$emit('izmena', {...konzola})">Izmeni</button><button
                    v-on:click="$emit('uklanjanje', konzola.id)" class="btn btn-danger">Ukloni</button></td>

    </tr>
    </tr>
</tbody>
</table>
    `
}