export default {
    props: ["konzola", "tekst"],
    emits: ["sacuvaj"],
    data() {
        return {
            novaKonzola: this.konzola ? {...this.konzola} : {}
        }
    },
    watch: {
         konzola: function(newValue, oldValue) {
             this.novaKonzola = {...this.konzola};
         }
    },
    template: `
    <form class="form-control" style="width: 500px;"  v-on:submit.prevent="$emit('sacuvaj', {...novaKonzola})">
        <div class="mb-3">
            <label class="form-label">Generacija</label> 
            <input type="text" v-model="novaKonzola.generacija_id" class="form-control" required>
        </div>
        <div class="mb-3">
        <label class="form-label">Naziv</label> 
            <input type="text" v-model="novaKonzola.naziv" class="form-control" required>
        </div>
        <div class="mb-3">
        <label class="form-label">Opis</label> 
        <textarea class="form-control" v-model="novaKonzola.opis"></textarea>
        </div>
        <div class="mb-3">
            <label class="form-label">Cena</label> 
            <input type="number" v-model="novaKonzola.cena" class="form-control" required>
        </div>

        <div class="mb-3" >
            <input type="submit" v-bind:value="tekst">
        </div>
    </form>
    `
}