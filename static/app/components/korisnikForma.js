export default {
    props: ["korisnik", "tekst"],
    emits: ["sacuvaj"],
    data() {
        return {
            noviKorisnik: this.korisnik ? {...this.korisnik} : {}
        }
    },
    watch: {
         korisnik: function(newValue, oldValue) {
             this.noviKorisnik = {...this.korisnik};
         }
    },
    template: `
    <form class="form-controll" v-on:submit.prevent="$emit('sacuvaj', {...noviKorisnik})">
        <div class="mb-3">
            <label class="form-label">Korisničko ime</label> 
            <input type="text" v-model="noviKorisnik.korisnicko_ime" class="form-control" required>
        </div>
        <div class="mb-3">
        <label class="form-label">Lozinka</label> 
            <input type="password" v-model="noviKorisnik.lozinka" class="form-control" required>
        </div>
        <div class="mb-3">
        <label class="form-label">Ime</label> 
            <input type="text" v-model="noviKorisnik.ime" class="form-control" required>
        </div>
        <div class="mb-3">
            <label class="form-label">Prezime</label> 
            <input type="text" v-model="noviKorisnik.prezime" class="form-control" required>
        </div>

        <div class="mb-3" style="margin-left: 250px;">
            <input type="submit" v-bind:value="tekst">
        </div>
    </form>
    `
}