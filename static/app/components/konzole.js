export default {
    template: `
<div>
    <h1 style="color: white; margin-top: 20px;">Konzole</h1>
    <h2>Pregled svih dostupnih Playstation konzola</h2>
    <h2>Moguća izmena, uklanjanje i dodavanje</h2>
    <konzola-forma v-on:sacuvaj="create" v-bind:tekst="'Dodaj'"></konzola-forma>
    <tabela-konzola v-bind:konzole="konzole" v-on:uklanjanje="remove" v-on:izmena="setZaIzmenu"></tabela-konzola>
    <div>
        <img src="ps.png" class="img-fluid" >
    </div>
</div>
    `,
    data() {
        return {
            konzole: [],
        }
    },
    methods: {
        refreshKonzole() {
            axios.get("api/konzole").then((response) => {
                this.konzole = response.data;
            });
        },
        
        create(konzola) {
            
            axios.post("api/konzole", konzola).then((response) => {
                this.refreshKonzole();
            });
        },
        update(konzola) {
            console.log(konzola);
            axios.put(`api/konzole/${konzola.id}`, konzola).then((response) => {
                this.refreshKonzole();
            });
        },
        remove(id) {
            axios.delete(`api/konzole/${id}`).then((response) => {
                this.refreshKonzole();
            });
        },
        setZaIzmenu(konzola) {
            this.$router.push(`/konzole/${konzola.id}`);
        }
    },
    created() {
        this.refreshKonzole();
    }
}