import Prodavnica from './components/prodavnica.js';
import TabelaKorisnika from './components/tabelaKorisnika.js';
import KorisnikForma from './components/korisnikForma.js';
import TabelaKonzola from './components/tabelaKonzola.js';
import Korisnici from './components/korisnici.js';
import Korisnik from './components/korisnik.js';
import Konzole from './components/konzole.js';
import Konzola from './components/konzola.js';
import Login from './components/login.js';
import Logout from './components/logout.js';
import Registracija from './components/registracija.js';
import KonzolaForma from './components/konzolaForma.js';
import Kupovine from './components/kupovine.js';
import Kupovina from './components/kupovina.js';
import TabelaKupovina from './components/tabelaKupovina.js';
import KupovinaForma from './components/kupovinaForma.js';
import Kupi from './components/kupi.js';

axios.interceptors.request.use(config => {
    let token = localStorage.getItem("token");
    Object.assign(config.headers, { "Authorization": `Bearer ${token}` });
    return config;
});


const router = VueRouter.createRouter({
    history: VueRouter.createWebHashHistory(),
    routes: [
        { path: "/", component: Login },
        { path: "/korisnici", component: Korisnici },
        { path: "/korisnici/:id", component: Korisnik },
        { path: "/logout", component: Logout },
        { path: "/konzole", component: Konzole },
        { path: "/konzole/:id", component: Konzola },
        { path: "/registracija", component: Registracija },
        { path: "/kupovine", component: Kupovine },
        { path: "/kupovine/:id", component: Kupovina },
        { path: "/kupi", component: Kupi }
    ],
});

const app = Vue.createApp(Prodavnica);
app.component('tabela-korisnika', TabelaKorisnika);
app.component('tabela-konzola', TabelaKonzola);
app.component('tabela-kupovina', TabelaKupovina);
app.component('konzola-forma', KonzolaForma);
app.component('korisnik-forma', KorisnikForma);
app.component('kupovina-forma', KupovinaForma);
app.use(router);
app.mount("#app");