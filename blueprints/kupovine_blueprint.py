import flask
import datetime
from flask import Blueprint

from flask_jwt_extended import jwt_required

from utils.db import mysql

kupovine_blueprint = Blueprint("kupovine_blueprint", __name__)

@kupovine_blueprint.route("")
@jwt_required()
def get_all_kupovine():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kupovina")
    kupovine = cursor.fetchall()
    return flask.jsonify(kupovine)

@kupovine_blueprint.route("<int:kupovina_id>")
def get_kupovina(kupovina_id):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kupovina WHERE id=%s", (kupovina_id,))
    kupovina = cursor.fetchone()
    if kupovina is not None:
        return flask.jsonify(kupovina)
    
    return "", 404

@kupovine_blueprint.route("", methods=["POST"])
@jwt_required()
def dodavanje_kupovine():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO kupovina(korisnik_id, konzola_id, kolicina, datumKupovine) VALUES(%(korisnik_id)s, %(konzola_id)s, %(kolicina)s, %(datumKupovine)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201

@kupovine_blueprint.route("<int:kupovina_id>", methods=["PUT"])
def izmeni_kupovinu(kupovina_id):
    kupovina = dict(flask.request.json)
    kupovina["kupovina_id"] = kupovina_id
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE kupovina SET korisnik_id=%(korisnik_id)s, konzola_id=%(konzola_id)s, kolicina=%(kolicina)s, datumKupovine=%(datumKupovine)s WHERE id=%(kupovina_id)s", kupovina)
    db.commit()
    cursor.execute("SELECT * FROM kupovina WHERE id=%s", (kupovina_id,))
    kupovina = cursor.fetchone()
    return flask.jsonify(kupovina)

@kupovine_blueprint.route("<int:kupovina_id>", methods=["DELETE"])
def ukloni_kupovinu(kupovina_id):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM kupovina WHERE id=%s", (kupovina_id, ))
    db.commit()
    return ""
