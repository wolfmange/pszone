import flask
from flask import Blueprint

from flask_jwt_extended import jwt_required

from utils.db import mysql

konzole_blueprint = Blueprint("konzole_blueprint", __name__)

@konzole_blueprint.route("")
@jwt_required()
def get_all_konzole():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM konzola")
    konzole = cursor.fetchall()
    for p in konzole:
        p["cena"] = float(p["cena"])
    return flask.jsonify(konzole)

@konzole_blueprint.route("<int:konzola_id>")
def get_konzola(konzola_id):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM konzola WHERE id=%s", (konzola_id,))
    konzola = cursor.fetchone()
    if konzola is not None:
        konzola["cena"] = float(konzola["cena"])
        return flask.jsonify(konzola)
    
    return "", 404

@konzole_blueprint.route("", methods=["POST"])
@jwt_required()
def dodavanje_konzola():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO konzola(generacija_id, naziv, opis, cena) VALUES( %(generacija_id)s, %(naziv)s, %(opis)s, %(cena)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201

@konzole_blueprint.route("<int:konzola_id>", methods=["PUT"])
def izmeni_konzolu(konzola_id):
    konzola = dict(flask.request.json)
    konzola["konzola_id"] = konzola_id
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE konzola SET naziv=%(naziv)s, opis=%(opis)s, cena=%(cena)s WHERE id=%(konzola_id)s", konzola)
    db.commit()
    cursor.execute("SELECT * FROM konzola WHERE id=%s", (konzola_id,))
    konzola = cursor.fetchone()
    konzola["cena"] = float(konzola["cena"])
    return flask.jsonify(konzola)

@konzole_blueprint.route("<int:konzola_id>", methods=["DELETE"])
def ukloni_konzolu(konzola_id):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM konzola WHERE id=%s", (konzola_id, ))
    db.commit()
    return ""