import flask
from flask import Blueprint
from flask_jwt_extended.utils import get_jwt
from flask_jwt_extended import create_access_token
from flask_jwt_extended import jwt_required

from utils.db import mysql

logout_blueprint = Blueprint("logout_blueprint", __name__)

@logout_blueprint.route("", methods=["GET"])
def logout():
    return "", 200