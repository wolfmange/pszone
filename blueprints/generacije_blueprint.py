import flask
from flask import Blueprint

from flask_jwt_extended import jwt_required

from utils.db import mysql

generacije_blueprint = Blueprint("generacije_blueprint", __name__)

@generacije_blueprint.route("")
@jwt_required()
def get_all_generacije():

    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM generacija")
    generacije = cursor.fetchall()
    return flask.jsonify(generacije)

@generacije_blueprint.route("<int:generacija_id>")
def get_generacija(generacija_id):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM generacija WHERE id=%s", (generacija_id,))
    generacija = cursor.fetchone()
    if generacija is not None:
        return flask.jsonify(generacija)
    
    return "", 404

