import flask
from flask import Blueprint
from flask_jwt_extended import create_access_token
from flask_jwt_extended import jwt_required
from flask_jwt_extended import JWTManager


from utils.db import mysql

kupi_blueprint = Blueprint("kupi_blueprint", __name__)



@kupi_blueprint.route("", methods=["POST"])
@jwt_required()
def kupi_konzolu():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO kupovina(korisnik_id, konzola_id, kolicina, datumKupovine) VALUES(%(korisnik_id)s, %(konzola_id)s, %(kolicina)s, %(datumKupovine)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201