import flask
from flask import Blueprint

from flask_jwt_extended import jwt_required

from utils.db import mysql

korisnici_blueprint = Blueprint("korisnici_blueprint", __name__)

@korisnici_blueprint.route("")
@jwt_required()
def get_all_korisnici():

    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM korisnik")
    korisnici = cursor.fetchall()
    return flask.jsonify(korisnici)

@korisnici_blueprint.route("<int:korisnik_id>")
def get_korisnik(korisnik_id):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM korisnik WHERE id=%s", (korisnik_id,))
    korisnik = cursor.fetchone()
    if korisnik is not None:
        return flask.jsonify(korisnik)
    
    return "", 404

@korisnici_blueprint.route("", methods=["POST"])
def dodavanje_korisnika():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO korisnik(korisnicko_ime, lozinka, ime, prezime) VALUES(%(korisnicko_ime)s, %(lozinka)s, %(ime)s, %(prezime)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201

@korisnici_blueprint.route("<int:korisnik_id>", methods=["PUT"])
def izmeni_korisnika(korisnik_id):
    korisnik = dict(flask.request.json)
    korisnik["korisnik_id"] = korisnik_id
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE korisnik SET korisnicko_ime=%(korisnicko_ime)s, lozinka=%(lozinka)s, ime=%(ime)s, prezime=%(prezime)s WHERE id=%(korisnik_id)s", korisnik)
    db.commit()
    cursor.execute("SELECT * FROM korisnik WHERE id=%s", (korisnik_id,))
    korisnik = cursor.fetchone()
    return flask.jsonify(korisnik)

@korisnici_blueprint.route("<int:korisnik_id>", methods=["DELETE"])
def ukloni_korisnika(korisnik_id):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM korisnik WHERE id=%s", (korisnik_id, ))
    db.commit()
    return ""
