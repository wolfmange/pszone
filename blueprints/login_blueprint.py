import flask
from flask import Blueprint
from flask_jwt_extended import create_access_token
from flask_jwt_extended import jwt_required
from flask_jwt_extended import JWTManager


from utils.db import mysql

login_blueprint = Blueprint("login_blueprint", __name__)

@login_blueprint.route("", methods=["POST"])
def login():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM korisnik WHERE korisnicko_ime=%(korisnicko_ime)s AND lozinka=%(lozinka)s", flask.request.json)
    korisnik = cursor.fetchone()
    if korisnik is not None:
        access_token = create_access_token(identity=korisnik["korisnicko_ime"])
        return flask.jsonify(access_token), 200
    return "", 403

