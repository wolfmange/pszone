import flask
from flask import Blueprint
from flask_jwt_extended import create_access_token
from flask_jwt_extended import jwt_required
from flask_jwt_extended import JWTManager


from utils.db import mysql

registracija_blueprint = Blueprint("registracija_blueprint", __name__)

@registracija_blueprint.route("", methods=["POST"])
def registracija():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO korisnik(korisnicko_ime, lozinka, ime, prezime) VALUES(%(korisnicko_ime)s, %(lozinka)s, %(ime)s, %(prezime)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201

