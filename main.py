import flask
from flask import Flask
from flask import session
from flask_jwt_extended.utils import get_jwt

from flask_jwt_extended import create_access_token
from flask_jwt_extended import jwt_required

from flask_jwt_extended import JWTManager

from utils.db import mysql

from blueprints.login_blueprint import login_blueprint
from blueprints.logout_blueprint import logout_blueprint
from blueprints.registracija_blueprint import registracija_blueprint
from blueprints.korisnici_blueprint import korisnici_blueprint
from blueprints.konzole_blueprint import konzole_blueprint
from blueprints.generacije_blueprint import generacije_blueprint
from blueprints.kupovine_blueprint import kupovine_blueprint
from blueprints.kupi_blueprint import kupi_blueprint


app = Flask(__name__, static_url_path="/")
app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = "root"
app.config["MYSQL_DATABASE_DB"] = "ps_zone"
app.config["JWT_SECRET_KEY"] = "lflkf fklefl2rorka"
# app.secret_key = "adx"

app.register_blueprint(korisnici_blueprint, url_prefix="/api/korisnici")
app.register_blueprint(konzole_blueprint, url_prefix="/api/konzole")
app.register_blueprint(login_blueprint, url_prefix="/api/login")
app.register_blueprint(logout_blueprint, url_prefix="/api/logout")
app.register_blueprint(registracija_blueprint, url_prefix="/api/registracija")
app.register_blueprint(generacije_blueprint, url_prefix="/api/generacija")
app.register_blueprint(kupovine_blueprint, url_prefix="/api/kupovine")
app.register_blueprint(kupi_blueprint, url_prefix="/api/kupi")


mysql.init_app(app)

jwt = JWTManager(app)

@app.route("/")
def home():
    return app.send_static_file("index.html")

